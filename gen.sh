#!/bin/bash

set -ex

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
SELF_DIR="${SELF%/*}"

IN_VIDEOS="$SELF_DIR/processed_input_videos"
IN_IMAGES="$SELF_DIR/input_images"
OUT_VIDEOS_DIR="$SELF_DIR/output"
OUT_FINAL_DIR="$SELF_DIR/output_with_audio"
TMP_DIR=$(mktemp -d -t ci-$(date +%Y-%m-%d-%H-%M-%S)-XXXXXXXXXX)

deep_fake() {
    outname="$(basename "$1")-$(basename "$2").mp4"
    docker run -it --rm --gpus all \
        -v "$1":/tmp/video.mp4:ro \
        -v "$2":/tmp/picture.jpg:ro \
        -v "$OUT_VIDEOS_DIR":/tmp/out \
        mcginty/deepfakematrix \
        python3 demo.py --config config/vox-256.yaml \
          --driving_video /tmp/video.mp4 \
          --source_image /tmp/picture.jpg \
          --checkpoint vox-cpk.pth.tar \
          --result_video "/tmp/out/$outname" \
          --relative --adapt_scale

    tmp_file="$TMP_DIR/$(basename "$1").wav"
    ffmpeg -i "$1" "$tmp_file"
    ffmpeg -i "$OUT_VIDEOS_DIR/$outname" -i "$tmp_file" -c:v copy -c:a aac "$OUT_FINAL_DIR/$outname"
    rm -f "$tmp_file"
}

mkdir -p "$OUT_VIDEOS_DIR"
mkdir -p "$OUT_FINAL_DIR"

for image in "$IN_IMAGES"/*
do
    for video in "$IN_VIDEOS"/*
    do
        deep_fake $video $image
    done
done

rm -rf "$TMP_DIR"
