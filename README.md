# sophday / deepfakematrix

this makes it easy to take a number of videos, a number of images, and
generate the permutations of all of them.

## howto

1. throw all your videos into `raw_input_videos/` they don't have to be 256x256
   cropped.
2. throw all your images into `input_images/`. they DO have to be 256x256.
   sorry.
3. run `face-align.sh` to extract and auto-crop the faces from the videos.
4. run `gen.sh` to generate the deep fakes :)