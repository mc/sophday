#!/bin/bash

set -ex

SELF="$(readlink -f "${BASH_SOURCE[0]}")"
SELF_DIR="${SELF%/*}"
IN_DIR="$SELF_DIR/raw_input_videos"
OUT_DIR="$SELF_DIR/processed_input_videos"

run_docker() {
    docker run -it --rm --gpus all -v "$IN_DIR:/tmp/input:ro" -v "$OUT_DIR:/tmp/output" mcginty/deepfakematrix "$@"
}

mkdir -p "$OUT_DIR"

for video in "$IN_DIR"/*
do
    run_docker python3 crop-video.py --inp "/tmp/input/$(basename "$video")"
done

